# 4. Используя встроенные функции реализуйте:
# Объединение 2-х кортежей вида: ('a', 'b', 'c', 'd', 'e') и (1, 2, 3, 4, 5)
# в словарь.
# Результат работы программы: {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
# Конвертация элементов списка ['1', '2', '3'] в числа.
# Результат работы программы: [1, 2, 3]

tupl1 = ('a', 'b', 'c', 'd', 'e')
tupl2 = (1, 2, 3, 4, 5)

lst_str = ['1', '2', '3']
lst_int = [int(i) for i in lst_str]
