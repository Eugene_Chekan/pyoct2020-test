# Свяжите переменную со списком который состоит из элементов от 1 до 10
# Извлеките из списка элементы стоящие на нечетных позициях
# Найдите максимальное число
# Найдите минимальное число
# Найдите сумму всех чисел заданного списка
# Из списка вида [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]].
#   Напечатайте только первые элементы вложенных списков, те 1 4 7.
#   При помощи цикла for.

var_list = [i for i in range(1, 11)]

evens = var_list[::2]
maxi_num = max(var_list)
mini_num = min(var_list)
total_sum = sum(var_list)
nested_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]]
first_elem_nested = [i[0] for i in nested_list]
