# 5. Напишите функцию, которая принимает строку и
# возвращает кол-во заглавных и строчных букв.
# 'The quick Brow Fox' =>
# Upper case characters: 3
# Lower case characters: 12

def count_upper_lower(string: str):
    up_count = 0
    low_count = 0
    for i in string:
        if i.islower():
            low_count += 1
        elif i.isupper():
            up_count += 1
        else:
            pass
    print(f'Upper case characters: {up_count}'
          f'\nLower case characters: {low_count}')
